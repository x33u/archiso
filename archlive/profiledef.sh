#!/usr/bin/env bash
# shellcheck disable=SC2034

iso_name="3numOS-fulliso-beta"
iso_label="ARCH_$(date +%Y%m)"
iso_publisher="Arch Linux <https://archlinux.org>"
iso_application="Arch Linux Live/Rescue CD"
iso_version="$(date +%Y.%m.%d)"
install_dir="arch"
#buildmodes=('keys')
##>buildmodes=('iso' 'keys')
buildmodes=('iso')
bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito'
           'uefi-ia32.grub.esp' 'uefi-x64.grub.esp'
           'uefi-ia32.grub.eltorito' 'uefi-x64.grub.eltorito')
##bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito')
#           'uefi-ia32.grub.esp' 'uefi-x64.grub.esp'
#           'uefi-ia32.grub.eltorito' 'uefi-x64.grub.eltorito')
arch="x86_64"
pacman_conf="pacman.conf"
airootfs_image_type="squashfs"
##>airootfs_image_type="squashfs+luks"
airootfs_image_tool_options=('-comp' 'xz' '-Xbcj' 'x86' '-b' '1M' '-Xdict-size' '1M')
#encryption_key="auto"
##>encryption_key="mykeyfile"
file_permissions=(
  ["/etc/shadow"]="0:0:400"
  ["/root"]="0:0:750"
  ["/root/.automated_script.sh"]="0:0:755"
  ["/usr/local/bin/choose-mirror"]="0:0:755"
  ["/usr/local/bin/Installation_guide"]="0:0:755"
  ["/usr/local/bin/livecd-sound"]="0:0:755"
  ["/etc/shadow"]="0:0:0400"
  ["/etc/gshadow"]="0:0:0400"
  ["/etc/skel/.config/bspwm/bspwmrc"]="0:0:0755"
  ["/etc/skel/.config/sxhkd/sxhkdrc"]="0:0:0755"
  ["/etc/skel/.scripts/pwgen.sh"]="0:0:0755"
  ["/usr/local/share/wallpaper/background.png"]="0:0:0644"
  ["/etc/sudoers"]="0:0:0440"
  ["/root/blackarch-strap.sh"]="0:0:0750"
  ["/etc/skel/.ssh/authorized_keys"]="0:0:0600"
  ["/etc/skel/.config/polybar/launch.sh"]="0:0:0750"
)

