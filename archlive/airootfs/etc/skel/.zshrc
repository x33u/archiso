##---
# zsh config
##---

## Autologin
if [ "$(tty)" = "/dev/tty1" ]; then
  startx
fi

## History file
HISTFILE=~/.zsh_historyfile
HISTSIZE=10000
SAVEHIST=10000

## Run TMUX
#if [ "$TMUX" = "" ]; then tmux; fi

############ STYLING ################

setprompt() {
  setopt prompt_subst

  if [[ -n "$SSH_CLIENT"  ||  -n "$SSH2_CLIENT" ]]; then
    p_host='%F{yellow}%M%f'
  else
    p_host='%F{green}%M%f'
  fi

  PS1=${(j::Q)${(Z:Cn:):-$'
    %F{cyan}[%f
    %(!.%F{red}%n%f.%F{yellow}%n%f)
    %F{cyan}%f
    #%F{cyan}@%f
    #${p_host}
    %F{cyan}][%f
    %F{cyan}%~%f
    %F{cyan}]%f
    %(!.%F{red}%#%f.%F{green}%#%f)
    " "
  '}}

  PS2=$'%_>'
  RPROMPT=$'${vcs_info_msg_0_}'
}
setprompt


## Colored Manpages
man() {
  env \
    LESS_TERMCAP_mb=$(printf "\e[1;31m") \
    LESS_TERMCAP_md=$(printf "\e[1;31m") \
    LESS_TERMCAP_me=$(printf "\e[0m") \
    LESS_TERMCAP_se=$(printf "\e[0m") \
    LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
    LESS_TERMCAP_ue=$(printf "\e[0m") \
    LESS_TERMCAP_us=$(printf "\e[1;32m") \
    man "$@"
}

## Sytax higlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

## Dir colorize
LS_COLORS='rs=0:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:tw=30;42:ow=34>'
export LS_COLORS

## Additional
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#ff00ff,bg=cyan,bold,underline"


########### FUNCTION ###############

## Bindkeys
# use strg+<-|->
# search history
bindkey '\e[A' history-beginning-search-backward
bindkey '\e[B' history-beginning-search-forward
# use standard keysettings
bindkey "\e[1~" beginning-of-line
bindkey "\e[4~" end-of-line
bindkey "\e[5~" beginning-of-history
bindkey "\e[6~" end-of-history
bindkey "\e[7~" beginning-of-line
bindkey "\e[3~" delete-char
bindkey "\e[2~" quoted-insert
bindkey "\e[5C" forward-word
bindkey "\e[5D" backward-word
bindkey "\e\e[C" forward-word
bindkey "\e\e[D" backward-word
bindkey "\e[1;5C" forward-word
bindkey "\e[1;5D" backward-word
bindkey "\e[8~" end-of-line
bindkey "\eOH" beginning-of-line
bindkey "\eOF" end-of-line
bindkey "\e[H" beginning-of-line
bindkey "\e[F" end-of-line

## Compinit
autoload -Uz compinit; compinit

## Zstyle
zstyle ':completion:*' menu select matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'

## Exports
# $PATH
export PATH=/usr/local/bin:/usr/sbin:/sbin:/usr/bin:/bin:/home/cc1p/.python:
# GOPATH
export GOPATH=$HOME/code/go/bin
# URxvt
export TERM=xterm-256color
# EDITOR
export EDITOR="/usr/bin/nano -c"
# jump between slashes
export WORDCHARS=''
# NPM config
export npm_config_prefix="$HOME/.local"

################### ALIAS ######################

## Alias
# pw generator
alias pw=~/.scripts/pwgen.sh
# ls
alias l="ls --color -lha"
# nano
alias nano="nano -0"
# cd
alias ..="cd .."
# mkdir
alias mkdir="mkdir -p"
# cp
alias cp="rsync -avP"
# git
alias gc="git clone"
# ranger
alias r="ranger"
# prevent reboot
alias reboot="echo 'try: /usr/sbin/reboot'"
alias poweroff="echo 'try: /usr/sbin/poweroff'"
alias shutdown="echo 'try: /usr/sbin/shutdown'"


# prompt
ZSH_THEME_GIT_PROMPT_PREFIX="%{$reset_color%}%{$fg[green]%}["
ZSH_THEME_GIT_PROMPT_SUFFIX="]%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%}*%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN=""

# show git branch/tag, or name-rev if on detached head
parse_git_branch() {
  (command git symbolic-ref -q HEAD || command git name-rev --name-only --no-undefined --always HEAD) 2>/dev/null
}

# show red star if there are uncommitted changes
parse_git_dirty() {
  if command git diff-index --quiet HEAD 2> /dev/null; then
    echo "$ZSH_THEME_GIT_PROMPT_CLEAN"
  else
    echo "$ZSH_THEME_GIT_PROMPT_DIRTY"
  fi
}

# if in a git repo, show dirty indicator + git branch
git_custom_status() {
  local git_where="$(parse_git_branch)"
  [ -n "$git_where" ] && echo "$(parse_git_dirty)$ZSH_THEME_GIT_PROMPT_PREFIX${git_where#(refs/heads/|tags/)}$ZSH_THEME_GIT_PROMPT_SUFFIX"
}

# show current rbenv version if different from rbenv global
rbenv_version_status() {
  local ver=$(rbenv version-name)
  [ "$(rbenv global)" != "$ver" ] && echo "[$ver]"
}

# put fancy stuff on the right
if which rbenv &> /dev/null; then
  RPS1='$(git_custom_status)%{$fg[red]%}$(rbenv_version_status)%{$reset_color%} $EPS1'
else
  RPS1='$(git_custom_status) $EPS1'
fi
