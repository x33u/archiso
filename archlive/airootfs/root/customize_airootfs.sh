# exit on error and undefined variables
set -eu

# storing the system journal in RAM
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

# disable pc speaker beep
echo "blacklist pcspkr" > /etc/modprobe.d/nobeep.conf

# tweak pacman.conf
#echo "Server = https://mirror.x33u.org/archlinux/$repo/os/$arch" > /etc/pacman.d/mirrorlist
sed -i '/Color/s/^#//g' /etc/pacman.conf
sed -i '/ParallelDownloads/s/^#//g' /etc/pacman.conf
echo "[multilib]" >> /etc/pacman.conf
echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf
#echo "Server = https://mirror.x33u.org/archlinux/$repo/os/$arch" > /etc/pacman.d/mirrorlist

## run blackarch strap script
#echo "==> BLACKARCH-REPO"
#bash /usr/local/bin/blackarch-strap.sh
#pacman -Syy --noconfirm
#pacman-key --init
#pacman-key --populate blackarch archlinux
#pacman -Fyy
#pacman-db-upgrade
##updatedb
#sync
#
# clean up
pacman -Sc --noconfirm

# run ungoogled-chromium strap script
#echo "==> UNGOOGLED-CHROMIUM"
#bash /usr/local/bin/ungoogled-chromium-strap.sh
# see for latest binary:
# https://software.opensuse.org/download/package?package=ungoogled-chromium&project=home:ungoogled_chromium
#pacman -U --noconfirm https://download.opensuse.org/repositories/home:/ungoogled_chromium/Arch/x86_64/ungoogled-chromium-105.0.5195.102-1-x86_64.pkg.tar.zst

# /etc
echo '3numOS' > /etc/arch-release


# gen locale
locale-gen "en_US.UTF-8"
