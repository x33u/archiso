#!/bin/bash

## == remove build path
echo "removing build path"
rm -rf archiso-tmp/

## == remove output iso
echo "removing output iso"
rm out/*.iso
