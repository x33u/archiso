#### 3numOS - archiso

Enumeration-OS is an custom [Arch Linux](https://archlinux.org/) build using `archiso` and [bspwm](https://github.com/baskerville/bspwm) with some additional repositories and packages. The project aim is to provide an fully functional non persistent ~~pentesting~~ administration playground.

Built-in Repositories:

- [BlackArch Linux](https://www.blackarch.org/)
- [openSUSE ungoogled chromium](https://software.opensuse.org/download/package?package=ungoogled-chromium&project=home:ungoogled_chromium)


Built-in Packages:

- [ungoogled chromium](https://github.com/ungoogled-software/ungoogled-chromium)
- [VSCodium](https://github.com/VSCodium/vscodium)
- [BlackArch Linux slim ISO](https://github.com/BlackArch/blackarch-iso/blob/d43c20e03bc261c42e268e9fd3394437f8bf1c05/slim-iso/packages.x86_64#L243)
- [some additional packages](https://codeberg.org/x33u/archiso/src/branch/developement/archlive/packages.x86_64)

Good to know:

- [4G cowspace on `grub` and `syslinux`](https://gitlab.archlinux.org/mkinitcpio/mkinitcpio-archiso/blob/master/docs/README.bootparams)


Fit deps and clone repo

```shell
# ensure that "archiso" package is n place
> pacman -S archiso

# clone repo and change into it
> git clone \
  https://codeberg.org/x33u/archiso \
  && cd archiso

# create ssh folder
> mkdir archlive/airootfs/etc/skel/.ssh

# paste your public ssh-key into or create a blank file in
> "./archlive/airootfs/etc/skel/.ssh/authorized_keys"
```

AUR stuff

```shell
# initialize AUR packages
> git submodule update --init --recursive

# jump into AUR directory custom-repo-aur/
> cd custom-repo-aur/

# run updater script and go back to root path
> ./update-aur-pkgs.sh

# go back to repo root
> cd ../

# set custom AUR repo path (do this only once)
> echo -e \
  "Server = file://$PWD/custom-repo-aur/database/" \
  >> ./archlive/pacman.conf
```

Build

```shell
# change to user root
> sudo su

# build iso
> mkarchiso -v -w archiso-tmp archlive

# before re-building and to remove all artifacts run
> sh helpers/cleanup-build.sh
```

Boot ISO

```shell
# boot iso
> run_archiso -i \
  out/foo-name-yyyy.mm.dd-x86_64.iso
```

> the password for the default user is `live`

Tweak live system space

```shell
# resize partition size live to 16GB
> mount -o remount,size=16G /run/archiso/cowspace
```

~~Get pre builds from: [mirror.x33u.org/3numOS](https://mirror.x33u.org/3numOS/)~~
