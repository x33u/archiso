#!/bin/bash

set -x

## == update and build pkgs from repo folder
for PKGs in pkgs/*;
  do
    if [ "$PKGs" != "update-aur-pkgs.sh" ]
    then
      cd "$PKGs" ; git pull ; makepkg -s -f && cd ../../ ; echo -e "fin with '${PKGs}'"
    fi
done

## == copy pkgs into database/
cp -rav ./pkgs/*/*.pkg.tar.zst ./database/.

## == remove build pkgs
# rm ./pkgs/*/*.pkg.tar.zst


## == create custom-repo
repo-add ./database/custom-repo-aur.db.tar.gz \
 ./database/*.pkg.tar.zst
