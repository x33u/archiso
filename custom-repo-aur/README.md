#### custom repo

add packages:

```shell
# from inside of root directory create directorys
> mkdir \
  custom-repo-aur/pkgs

# add aur repo into pkgs/ directory
> git submodule \
  add https://aur.archlinux.org/cava.git \
  custom-repo-aur/pkgs/cava
```


automatically adding packages to db:
```shell
# jump into custom-repo-aur/
> cd custom-repo-aur

# run update script
> ./update-aur-pkgs.sh
```

manually adding packages to db
```shell
# make package
> cd custom-repo-aur/pkgs/cava && makepkg

# move pkg into database/
> mv ./custom-repo-aur/pkgs/*/*.pkg.tar.zst ./custom-repo-aur/database/.

# create custom-repo
> repo-add ./custom-repo-aur/database/custom-repo-aur.db.tar.gz \
 ./custom-repo-aur/database/*.pkg.tar.zst
```
